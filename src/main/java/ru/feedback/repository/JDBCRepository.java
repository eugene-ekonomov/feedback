package ru.feedback.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.feedback.model.Message;
import ru.feedback.model.User;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class JDBCRepository {

    private static JDBCRepository instance = new JDBCRepository();

    private static final Logger log = LogManager.getLogger(JDBCRepository.class);
    private DataSource ds;

    private JDBCRepository(){
        HikariConfig config = new HikariConfig("/sql/postgres.properties");
        ds = new HikariDataSource(config);
//        log.info("Подключение к базе данных: " + config.getDataSourceProperties().getProperty("serverName"));
    }

    public static JDBCRepository getInstance(){
        return instance;
    }

    public boolean setUser(String login, String password, String email, String lastName, String firstName, String fathersName) {
        try (Connection conn = ds.getConnection();){

            String SQL = " INSERT INTO users(login, password, email, last_name, first_name, father_s_name)  "
                    + " VALUES(?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, lastName);
            preparedStatement.setString(5, firstName);
            preparedStatement.setString(6, fathersName);

            int affectedRows = preparedStatement.executeUpdate();

            preparedStatement.close();

            SQL = " INSERT INTO user_roles(login, role_name)  "
                    + " VALUES(?, ?)";
            preparedStatement = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, "user");
            preparedStatement.executeUpdate();

            preparedStatement.close();

            if (affectedRows > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException ex) {
            log.error("SQLException: " + ex.getMessage());
        }
        return false;
    }

    public User getUser(String login){
        try (Connection conn = ds.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement(
                    " select    usr.login," +
                            "       usr.email," +
                            "       usr.last_name," +
                            "       usr.first_name," +
                            "       usr.father_s_name\n" +
                            "from users usr\n" +
                            "where usr.login=?");

            preparedStatement.setString(1, login);

            ResultSet result = preparedStatement.executeQuery();
            result.next();

            User user = new User(result.getString(1),
                    result.getString(2),
                    result.getString(3),
                    result.getString(4),
                    result.getString(5));

            result.close();
            preparedStatement.close();

            return user;

        }catch (SQLException ex){
            log.error("SQLException: " + ex.getMessage());
        }
        return null;
    }
    public List<User> getAdmins(){
        try (Connection conn = ds.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement(
                    " select    usr.login," +
                            "       usr.email," +
                            "       usr.last_name," +
                            "       usr.first_name," +
                            "       usr.father_s_name\n" +
                            "from users usr\n" +
                            "join user_roles ur on usr.login=ur.login\n" +
                            "where ur.role_name='Оператор'");

            ResultSet result = preparedStatement.executeQuery();

            ArrayList<User> users = new ArrayList<>();
            while(result.next()){
                users.add(new User(result.getString(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5)));
            }

            result.close();
            preparedStatement.close();

            return users;

        }catch (SQLException ex){
            log.error("SQLException: " + ex.getMessage());
        }
        return null;
    }

    public boolean addMessage(String sender, String receiver, String subject, String messageText) {
        try (Connection conn = ds.getConnection();){

            String SQL = " INSERT INTO messages(subject, text, sender_login, receiver_login)  "
                       + " VALUES(?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, subject);
            preparedStatement.setString(2, messageText);
            preparedStatement.setString(3, sender);
            preparedStatement.setString(4, receiver);

            int affectedRows = preparedStatement.executeUpdate();

            preparedStatement.close();

            if (affectedRows > 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException ex) {
            log.error("SQLException: " + ex.getMessage());
        }
        return false;

    }

    public ArrayList<Message> getMessages(String lastName, String firstName, String fathersName, String receiverLastName, String receiverFirstName, String receiverFathersName) {
        try (Connection conn = ds.getConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement(
                    " select snd.last_name || ' ' || snd.first_name || ' ' || snd.father_s_name || ' (' || snd.email || ')',\n" +
                            "       rcv.last_name || ' ' || rcv.first_name || ' ' || rcv.father_s_name || ' (' || rcv.email || ')',\n" +
                            "       msg.subject,\n" +
                            "       msg.text\n" +
                            "from messages msg\n" +
                            "join users snd on msg.sender_login=snd.login\n" +
                            "join users rcv on msg.receiver_login=rcv.login\n" +
                            "where (snd.last_name like ? or ? is null)\n" +
                            "  and (snd.first_name like ? or ? is null)\n" +
                            "  and (snd.father_s_name like ? or ? is null)\n" +
                            "  and (rcv.last_name like ? or ? is null)\n" +
                            "  and (rcv.first_name like ? or ? is null)\n" +
                            "  and (rcv.father_s_name like ? or ? is null)");
            preparedStatement.setString(1, lastName+"%");
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, firstName+"%");
            preparedStatement.setString(4, firstName);
            preparedStatement.setString(5, fathersName+"%");
            preparedStatement.setString(6, fathersName);
            preparedStatement.setString(7, receiverLastName+"%");
            preparedStatement.setString(8, receiverLastName);
            preparedStatement.setString(9, receiverFirstName+"%");
            preparedStatement.setString(10, receiverFirstName);
            preparedStatement.setString(11, receiverFathersName+"%");
            preparedStatement.setString(12, receiverFathersName);
            ResultSet result = preparedStatement.executeQuery();

            ArrayList<Message> messages = new ArrayList<Message>();
            while(result.next()){
                messages.add(new Message(result.getString(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4)));
            }

            result.close();
            preparedStatement.close();

            return messages;
        }catch (SQLException ex){
            log.error("SQLException: " + ex.getMessage());
        }
        return null;
    }
}