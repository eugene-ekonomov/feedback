package ru.feedback.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.feedback.repository.JDBCRepository;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/sendFeedbackServlet")
public class SendFeedbackServlet  extends HttpServlet {

    static final Logger log = LogManager.getLogger(SendFeedbackServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sender = request.getParameter("sender");
        String receiver = request.getParameter("receiver");
        String subject = request.getParameter("subject");
        String messageText = request.getParameter("messageText");

        JDBCRepository rep = JDBCRepository.getInstance();
        boolean result = rep.addMessage(sender, receiver, subject, messageText);
        request.setAttribute("result", result);

        request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);

    }
}

