package ru.feedback.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.Registration;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import ru.feedback.model.User;
import ru.feedback.repository.JDBCRepository;


@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    static final Logger log = LogManager.getLogger(Registration.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("pass");
        String email = request.getParameter("email");
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String fathersName = request.getParameter("fathersName");
        JDBCRepository rep = JDBCRepository.getInstance();
        boolean registered = rep.setUser(login, password, email, lastName, firstName,fathersName);

        String result = "Пользователь " + login + (registered ? " был": " не был") + " зарегистрирован.";
        log.info(result);
        request.setAttribute("result", result);
        request.setAttribute("registered", registered);
        request.getRequestDispatcher("WEB-INF/result.jsp").forward(request, response);
    }
}
