package ru.feedback.web;

import ru.feedback.model.User;
import ru.feedback.repository.JDBCRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/feedbackservlet")
public class FeedbackServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getRemoteUser();

        JDBCRepository rep = JDBCRepository.getInstance();
        User user = rep.getUser(username);
        request.getSession().setAttribute("user", user);
        List<User> admins = rep.getAdmins();
        request.getSession().setAttribute("admins", admins);
        request.getRequestDispatcher("feedback.jsp").forward(request, response);
    }
}
