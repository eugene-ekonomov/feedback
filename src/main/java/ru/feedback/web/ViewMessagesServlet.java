package ru.feedback.web;

import ru.feedback.model.Message;
import ru.feedback.repository.JDBCRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static ru.feedback.web.RegistrationServlet.log;

@WebServlet("/viewMessages")
public class ViewMessagesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String fathersName = request.getParameter("fathersName");
        String receiverLastName = request.getParameter("receiverLastName");
        String receiverFirstName = request.getParameter("receiverFirstName");
        String receiverFathersName = request.getParameter("receiverFathersName");
        JDBCRepository rep = JDBCRepository.getInstance();
        ArrayList<Message> messages = rep.getMessages(lastName, firstName,fathersName,receiverLastName, receiverFirstName,receiverFathersName);

        log.info(messages.toString());
        request.setAttribute("messages", messages);
        request.getRequestDispatcher("/WEB-INF/messages.jsp").forward(request, response);
    }
}