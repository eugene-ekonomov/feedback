DELETE FROM users;
DELETE FROM user_roles;



INSERT INTO users(login, password, email, last_name, first_name, father_s_name) VALUES
  ('ivanov', 'ivanov', 'ivanov@localhost.ru', 'Иванов', 'Иван', 'Иванович'),
  ('petrov', 'petrov', 'petrov@localhost.ru', 'Петров', 'Пётр', 'Петрович'),
  ('sidorov', 'sidorov', 'sidorov@localhost.ru', 'Сидоров', 'Сидор', 'Сидорович'),
  ('karlson', 'karlson', 'karlson@localhost.ru', 'Карлсон', 'Карл', 'Карлович');

INSERT INTO user_roles(login, role_name) VALUES
  ('ivanov', 'user'),
  ('petrov', 'Оператор'),
  ('sidorov', 'Оператор'),
  ('karlson', 'user');