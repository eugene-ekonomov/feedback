DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS user_roles CASCADE;

create table users (
  login             varchar not null primary key,
  password          varchar,
  email             varchar,
  last_name         varchar,
  first_name        varchar,
  father_s_name     varchar
);

create table user_roles (
  login             varchar not null,
  role_name         varchar not null,
  primary key (login, role_name)
);

create table messages(
  id             integer primary key,
  subject        varchar,
  text           varchar,
  sender_login   varchar,
  receiver_login varchar,
  FOREIGN KEY (sender_login) REFERENCES users(login),
  FOREIGN KEY (receiver_login) REFERENCES users(login)

)