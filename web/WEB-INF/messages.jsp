<?xml version="1.0" encoding="UTF-8"?>
<%@page contentType="application/xml" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<data>
    <table>
        <tr>
            <th>Отправитель</th>
            <th>Получатель</th>
            <th>Тема</th>
            <th>Сообщение</th>
        </tr>
        <c:forEach items="${messages}" var="message">
            <tr>
                <td>${message.sender}</td>
                <td>${message.receiver}</td>
                <td>${message.subject}</td>
                <td>${message.text}</td>
            </tr>
        </c:forEach>
    </table>
</data>