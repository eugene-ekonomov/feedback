$("#button").on('click', function() {
    var login=document.getElementById('login').value;
    var pass=document.getElementById('pass').value;
    var email=document.getElementById('email').value;
    var lastName=document.getElementById('lastName').value;
    var firstName=document.getElementById('firstName').value;
    var fathersName=document.getElementById('fathersName').value;

    if(login=="" || pass=="" || email=="" || lastName=="" || firstName==""){
        $("#error").html("<div class='text'>Заполните все обязательные поля (отчество может отсутствовать).</div>");
        return;
    }

    $.ajax({
        method: "GET",
        url: "registration",
        data: "login="+login+"&pass="+pass+"&email="+email+"&lastName="+lastName+"&firstName="+firstName+"&fathersName="+fathersName, // если сервер что-то получает
        success: function (responseXml) {
            if($(responseXml).find("registered").html()=="true"){
                document.location.href = "login.jsp";
            }else{
                $("#error").html($(responseXml).find("result").html());
            }

        }
    });
});