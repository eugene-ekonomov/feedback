$("#button").on('click', function() {
    var sender=document.getElementById('login').value;
    var receiver=document.getElementById('receiver').value;
    var subject=document.getElementById('subject').value;
    var messageText=document.getElementById('messageText').value;

    if(subject=="" || messageText==""){
        $("#result").html("<div class='text'>Заполните тему и сообщение.</div>");
        return;
    }

    $.ajax({
        method: "GET",
        url: "sendFeedbackServlet",
        data: "sender="+sender+"&receiver="+receiver+"&subject="+subject+"&messageText="+messageText, // если сервер что-то получает
        success: function (responseXml) {
            document.getElementById('receiver').value="";
            document.getElementById('subject').value="";
            document.getElementById('messageText').value="";
        }
    });
});