$("#button").on('click', function() {
    var lastName=document.getElementById('lastName').value;
    var firstName=document.getElementById('firstName').value;
    var fathersName=document.getElementById('fathersName').value;
    var receiverLastName=document.getElementById('receiverLastName').value;
    var receiverFirstName=document.getElementById('receiverFirstName').value;
    var receiverFathersName=document.getElementById('receiverFathersName').value;

    if(lastName=="" && firstName=="" && fathersName=="" && receiverLastName=="" && receiverFirstName=="" && receiverFathersName==""){
        $("#res").html("<div class='text'>Заполните хотя-бы одно поле для поиска</div>");
        return;
    }

    $.ajax({
        method: "GET",
        url: "viewMessages",
        data: "lastName="+lastName+"&firstName="+firstName+"&fathersName="+fathersName+"&receiverLastName="+receiverLastName+"&receiverFirstName="+receiverFirstName+"&receiverFathersName="+receiverFathersName,
        success: function (responseXml) {
            $("#res").html($(responseXml).find("data").html());
        }
    });
});