<%--
  Created by IntelliJ IDEA.
  User: X
  Date: 24.08.2018
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Feedback</title>
    <link rel="stylesheet" type="text/css" href="CSS/centre.css">
</head>
<body>
    <div class="ctr">
        <a href="feedbackservlet">Форма обратной связи</a>
        <br/>

        <c:if test='${pageContext.request.isUserInRole("Оператор")}'>
            <a href="viewmessages.jsp">Просмотр</a>
        </c:if>
    </div>
</body>
</html>