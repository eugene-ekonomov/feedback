<%--
  Created by IntelliJ IDEA.
  User: X
  Date: 24.08.2018
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, max-age=0, must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="Fri, 01 Jan 1990 00:00:00 GMT"/>
  <title>Search</title>
  <link rel="stylesheet" type="text/css" href="CSS/feedback_style.css">
</head>
<body>

<section id="main">
  <a href="logout.jsp" >Выход из аккаунта </a>${pageContext.request.remoteUser}<br>

  <a class="text" href="/feedback">Главная страница</a>

  <div id="fields">
    <div id = "feedbackLabel">Форма обратной связи</div>

    <input id='login' type="hidden" value=${user.login}>

    <div class="text">Фамилия</div>

    <input id='lastName' type='text' value=${user.lastName} disabled>

    <div class="text">Имя</div>

    <input id='firstName' type='text' value=${user.firstName} disabled>

    <div class="text">Отчество</div>

    <input id='fathersName' type='text' value=${user.fathersName} disabled>

    <div class="text">Получатель</div>

    <select id='receiver' >
      <option disabled>Выберите получателя</option>
      <c:forEach items="${admins}" var="admin">
        <option value="${admin.login}">${admin.lastName} ${admin.firstName} ${admin.fathersName} &lt;${admin.email}&gt;</option>
      </c:forEach>
    </select>

    <div class="text">Тема</div>

    <input id='subject' type='text' value=""/>

    <textarea id='messageText' cols="40" rows="10"></textarea>

    <input id="button" type="button" value="Отправить"/>
  </div>
  <br /><br />
  <div id='result'> </div>

</section>


<script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="scripts/feedback.js"></script>


</body>
</html>